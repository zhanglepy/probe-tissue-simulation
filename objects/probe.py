import Sofa.Core
import Sofa.SofaDeformable

import numpy as np
from typing import Optional, List

from components.tetrahedral import Topology, add_collision_models, add_loader, add_tetrahedral_forcefield, add_topology, add_visual_models


class Probe(Sofa.Core.Controller):
    """ Sofa Controller representing an ultrasound probe """
    def __init__(
        self,
        root_node: Sofa.Core.Node,
        visual_filename: Optional[str] = None,
        collision_filename: Optional[str] = None,
        node_name: str = "Probe",
        init_pose: list = [0,0,0,0,0,0,1],
        target_position: np.ndarray = np.zeros((1,3)),
        velocity: float = 0.01
        ):
        """ 
        Args:
            root_node (Sofa.Core.Node): root node of the simulation.
            visual_filename (str, Optional): name of the triangular surface file used for visualization.
            collision_filename (str): name of the triangular surface file used for collision computation.
            node_name (str): name of the created node.
            init_pose (list): initial position and orientation of the probe, expressed as [x, y, z, qx, qy, qz, qw]
            target_position (list): target position to reach in case predefined motion is performed (in probe reference frame), expressed as [x, y, z]
        """

        Sofa.Core.Controller.__init__(self)
        
        self.init_pose = init_pose
        self.current_pose = None
        self.motion_path = []
        self.velocity = velocity
        self.root_node = root_node

        probe_node = root_node.addChild(node_name)
        self.state = probe_node.addObject('MechanicalObject',
                                            name='probe_state',
                                            position=init_pose,
                                            template="Rigid3d",
                                            showObject=1,
                                            showObjectScale=0.01,
                                            listening=1
                                            )

        # add_solver( parent_node=probe_node, add_constraint_correction=False )
        # probe_node.addObject('UniformMass',
                        # totalMass=0.1,
                        # name='probe_mass'
                        # )
        # probe_node.addObject('UncoupledConstraintCorrection')

        # Add collision models
        probe_collision = probe_node.addChild(f"{node_name}Collision")
        collision_loader = add_loader( parent_node=probe_collision,
                                         filename=collision_filename,
                                         name='probe_collision_loader',
                                        )

        add_topology( parent_node=probe_collision, 
                      mesh_loader=collision_loader,
                      topology=Topology.TRIANGLE
                    )

        self.collision_state = probe_collision.addObject('MechanicalObject',
                                name='probe_state',
                                template='Vec3d',
                                showObject=1,
                                showObjectScale=3,
                                listening=1
                                )
        
        add_collision_models( parent_node=probe_collision) #, spheres=True, radius=0.002 )
        probe_collision.addObject('RigidMapping', name='CollisionMapping')

        # Add visual models
        visual_node = probe_node.addChild(f"Visual{node_name}")
        visual_loader = add_loader( parent_node=visual_node,
                                    filename=visual_filename,
                                    name='probe_visual_loader',
                                    )

        add_topology( parent_node=visual_node, 
                      mesh_loader=visual_loader,
                      topology=Topology.TRIANGLE
                    )

        add_visual_models( parent_node = visual_node,
                            color = [0.957, 0.730, 0.582, 0.99]
                           )

        visual_node.addObject('RigidMapping', name='VisualMapping') #, input=, output=)

        # Create probe motion
        self.target_position = target_position
        self.num_deformations = target_position.shape[0]
        print(f"Number of deformations to apply: {self.num_deformations}")
        self.current_def = 1
        if self.num_deformations > 1:
            target_position = target_position[self.current_def-1,:]
        self.motion_path = self.create_linear_motion( target_position=target_position, dt=root_node.dt.value, velocity=self.velocity)

	#########################################################
	####### SOFA-RELATED
	#########################################################

    def onAnimateBeginEvent(self, __):
        if len(self.motion_path):  # if there is a motion path, execute one step
            self.current_def_ended = False
            new_pose = np.append(self.motion_path.pop(0), self.init_pose[3:])
            self.state.position.value = [new_pose]
        elif self.current_def <= self.num_deformations:
            self.current_def_ended = True
            print(f"End of deformation {self.current_def}")
            self.current_def += 1
            if self.current_def <= self.num_deformations:
                target_position = self.target_position[self.current_def-1,:]
                self.motion_path = self.create_linear_motion( target_position=target_position, dt=self.root_node.dt.value, velocity=self.velocity)
            
	#########################################################
	####### CUSTOM
	#########################################################
    def create_linear_motion(self, target_position: np.ndarray, dt: float, velocity: float, single_step: bool = False, start_position: Optional[np.ndarray] = None) -> List[np.ndarray]:
        """
        Creates movement path to displace probe from its current position to the final position provided, at the given velocity.
        """

        if start_position is None:
            current_position = self.state.position.value[0]
            # only use xyz
            current_position = current_position[:3]
        else:
            assert len(start_position) == 3
            current_position = start_position

        displacement = target_position - current_position
        print(f"Now moving to {target_position}, from {current_position}")

        motion_path = []

        if single_step:
            motion_path = [target_position]
            motion_steps = 1
        else:
            displacement_per_step = velocity * dt
            motion_steps = int(np.ceil(np.linalg.norm(displacement) / displacement_per_step))

            progress = np.linspace(0.0, 1.0, motion_steps + 1)[1:]
            motion_path = current_position + displacement * progress[:, np.newaxis]

            motion_path[-1] = target_position

        return np.split(motion_path, motion_steps, axis=0)

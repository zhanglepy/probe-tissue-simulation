import Sofa.Core
import Sofa.SofaDeformable

from typing import Optional
import numpy as np

from components.solver import SolverType, ConstraintCorrectionType, add_solver
from components.tetrahedral import Topology, add_collision_models, add_loader, add_tetrahedral_forcefield, add_topology, add_visual_models

class Breast(Sofa.Core.Controller):
	""" Sofa Controller representing the breast """
	def __init__(
		self,
		root_node: Sofa.Core.Node,
		volume_filename: str,
		collision_filename: str,
		init_tf: np.ndarray = np.identity(4),
		density: float = 1000,
		material: str = "Corotated",
		E: float = 3000,
		nu: float = 0.45,
		fixed_indices: Optional[list] = None,
		node_name: str = "Breast",
		visual_filename: Optional[str] = None,
		):
		""" 
		Args:
			root_node (Sofa.Core.Node): root node of the simulation.
			volume_filename (str): name of the tetrahedral volume file.
			collision_filename (str): name of the triangular surface file used for collision computation.
			init_tf (np.ndarray): initial 4x4 transformation to be applied to the model.
			density (float): object density.
			material (str): type of material.
			E (float): elastic modulus of the object.
			nu (float): poisson ratio.
			fixed_indices (list): indices of the volume mesh which are constrained in all directions.
			node_name (str): name of the created node.
			visual_filename (str, Optional): name of the triangular surface file used for visualization.

		"""
		Sofa.Core.Controller.__init__(self)

		breast_node = root_node.addChild(node_name)

		# Add mechanical model
		volume_loader = add_loader( parent_node=breast_node,
									filename=volume_filename,
									name='breast_volume_loader',
									transformation=init_tf
									)

		self.topology = add_topology( parent_node=breast_node, 
									  mesh_loader=volume_loader,
									  topology=Topology.TETRAHEDRON
									)

		self.state = breast_node.addObject('MechanicalObject',
								name='breast_state',
								template='CompressedRowSparseMatrixMat3x3d',
								showObject=0,
								listening=1
								)

		breast_node.addObject('MeshMatrixMass',
								massDensity=density,
								name='breast_mass'
								)
				
		add_tetrahedral_forcefield( parent_node=breast_node,
									material = material,
									E = E,
									nu = nu
									)

		add_solver( parent_node=breast_node, 
					solver_type=SolverType.SOFASPARSE,
					add_constraint_correction=True, 
					constraint_correction=ConstraintCorrectionType.LINEAR
					)

		# Use fixed points from the box ONLY if no fixed indices have been provided
		if fixed_indices is None:
			# Define a box for fixed points
			xmin, xmax, ymin, ymax, zmin, zmax = get_bbox( self.topology.position.value )
			box_fixed  = [ xmin, ymin, zmax, xmax, ymax, zmin+0.006 ]
			breast_node.addObject('BoxROI',
								name='fixed_points_box',
								box=box_fixed,
								drawPoints=True,
								drawSize=0.01
								)
			fixed_indices = '@fixed_points_box.indices'

		breast_node.addObject('FixedConstraint',
								name='fixed_points',
								indices=fixed_indices,
								)

		# Add visual models
		if not visual_filename is None:
			visual_node = breast_node.addChild(f"Visual{node_name}")
			visual_loader = add_loader( parent_node=visual_node,
										filename=visual_filename,
										name='breast_visual_loader',
										transformation=init_tf
									   )

			add_topology( parent_node=visual_node, 
						  mesh_loader=visual_loader,
						  topology=Topology.TRIANGLE
						)
			
			add_visual_models( parent_node = visual_node,
								color = [0.957, 0.730, 0.582, 0.99]
								)
			visual_node.addObject('BarycentricMapping', name='VisualMapping')
		
		# Add collision models
		collision_node = breast_node.addChild(f"Collision{node_name}")
		collision_loader = add_loader( parent_node=collision_node,
									   filename=collision_filename,
									   name='breast_collision_loader',
									   transformation=init_tf
									 )
		
		add_topology( parent_node=collision_node, 
					  mesh_loader=collision_loader,
					  topology=Topology.TRIANGLE
					)

		collision_node.addObject('MechanicalObject', 
									name='breast_collision_state',
									template='Vec3d',
									showObject=0,
									listening=1
									)

		add_collision_models( parent_node=collision_node )

		collision_node.addObject('BarycentricMapping', 
									name='CollisionMapping'
									)
		
		self.previous_position = self.topology.position.value
		self.node = breast_node
		self.is_stable = True


	def onSimulationInitDoneEvent(self, _):
		pass

	def onAnimateEndEvent(self, __):
		# Check simulation instability
		current_position = self.state.position.value
		breast_displacement = current_position - self.previous_position
		self.is_stable = is_stable(breast_displacement)
		if not self.is_stable:
			print("Simulation is UNSTABLE")
		

class Lesion(Sofa.Core.Controller):
	""" Sofa Controller representing a point mapped to a parent deformable object"""
	def __init__(
		self,
		parent_node: Sofa.Core.Node,
		lesion_position: np.ndarray = np.zeros((1,3)),
		surface_filename: Optional[str] = None,
		init_tf: np.ndarray = np.identity(4),
		node_name: str = "Lesion",
		):
		""" 
		Args:
			parent_node (Sofa.Core.Node): node of the object the lesion must be attached to.
			surface_filename (str): name of the surface file describing the lesion.
			init_tf (np.ndarray): initial 4x4 transformation to be applied to the model.
			node_name (str): name of the created node.
		"""

		Sofa.Core.Controller.__init__(self)

		lesion_node = parent_node.addChild(node_name)

		if not surface_filename is None:
			surface_loader = add_loader( parent_node=lesion_node,
										filename=surface_filename,
										name='lesion_surface_loader',
										transformation=init_tf
										)

			self.topology = add_topology( parent_node=lesion_node, 
										  mesh_loader=surface_loader,
										  topology=Topology.TRIANGLE
										)

			self.state = lesion_node.addObject('MechanicalObject',
													name='FiducialMecha',
													showObjectScale=2,
													template='Vec3d',
													showObject=1,
													listening=1,
													showColor=[0, 1, 0, 1]) 

		else:
			self.state = lesion_node.addObject('MechanicalObject',
													name='FiducialMecha',
													position=lesion_position,
													showObjectScale=10,
													template='Vec3d',
													showObject=1,
													listening=1,
													showColor=[0, 1, 0, 1])

		lesion_node.addObject('BarycentricMapping') 

		self.node = lesion_node


	def onSimulationInitDoneEvent(self, _):
		pass

	def onAnimateEndEvent(self, __):
		pass

def get_bbox(position):
	""" 
	Gets the bounding box of the object defined by the given vertices.

	Arguments
	-----------
	position : list
		List with the coordinates of N points.

	Returns
	----------
	xmin, xmax, ymin, ymax, zmin, zmax : floats
		min and max coordinates of the object bounding box.
	"""
	points_array = np.asarray(position)
	m = np.min(points_array, axis=0)
	xmin, ymin, zmin = m[0], m[1], m[2]

	m = np.max(points_array, axis=0)
	xmax, ymax, zmax = m[0], m[1], m[2]

	return xmin, xmax, ymin, ymax, zmin, zmax

def is_stable(displacement):
	""" 
	Analyzes the provided displacement and tells if the displacement is associated with 
	a stable deformation (i.e., lower than high_thresh).

	Arguments
	-----------
	displacement : array_like
		Nx3 array with x,y,z displacements of N points.
	
	Returns
	-----------
	bool
		False if there is at least one displacement with NaN value
	"""

	displ_norm = np.linalg.norm(displacement, axis=1)
	max_displ_norm = np.amax(displ_norm)

	if np.isnan(max_displ_norm):
		return False
	else:
		return True